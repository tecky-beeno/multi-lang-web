function changeLang(lang) {
  localStorage.lang = lang;
  applyLang();
}
function getLangs() {
  if (localStorage.lang === 'en') {
    return en;
  }
  return zh;
}
function applyLang() {
  let lang = getLangs();
  document.querySelectorAll('[word]').forEach(e=>{
    let word = e.getAttribute('word')
    e.textContent = lang[word]
  })
}
setTimeout(() => {
  applyLang();
});
