let en = {
  title: 'Home Page',
  desc: 'Welcome to my page',
  uptime: 'Up Time',
};
let zh = {
  title: '主頁',
  desc: '歡迎來到我的網站',
  uptime: '運行時間',
};

let uptime = document.querySelector('#uptime');
setInterval(() => {
  let time = document.timeline.currentTime / 1000;
  time = Math.floor(time);
  uptime.textContent = time + ' seconds';
});
